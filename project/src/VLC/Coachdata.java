package VLC;

public class Coachdata {


	String Coach_id_tf, Fname_tf, Lname_tf, Parent_first_name_tf, Parent_last_name_tf, Dob_dc, Date_of_admission_dc, Gender_group, Email_tf, phone_no_tf, Street_no_tf, House_no_tf, Zip_code_tf, Class_to_attend_cb, room_cb;

	public Coachdata(String Coach_id_tf, String Fname_tf, String Lname_tf, String Parent_first_name_tf, String Parent_last_name_tf, String Dob_dc, String Date_of_admission_dc, String Gender_group, String Email_tf, String phone_no_tf, String Street_no_tf, String House_no_tf, String Zip_code_tf, String Class_to_attend_cb, String room_cb) {
		super();
		this.Coach_id_tf = Coach_id_tf;
		this.Fname_tf = Fname_tf;
		this.Lname_tf = Lname_tf;
		this.Parent_first_name_tf = Parent_first_name_tf;
		this.Parent_last_name_tf = Parent_last_name_tf;
		this.Dob_dc = Dob_dc;
		this.Date_of_admission_dc = Date_of_admission_dc;
		this.Gender_group = Gender_group;
		this.Email_tf = Email_tf;
		this.phone_no_tf = phone_no_tf;
		this.Street_no_tf = Street_no_tf;
		this.House_no_tf = House_no_tf;
		this.Zip_code_tf = Zip_code_tf;
		this.Class_to_attend_cb = Class_to_attend_cb;
		this.room_cb = room_cb; 
	}

	public String getCoach_id() {
		return Coach_id_tf;
	}

	public void setCoach_id(String Coach_id_tf) {
		this.Coach_id_tf = Coach_id_tf;
	}

	public String getFname() {
		return Fname_tf;
	}

	public void setFname(String Fname_tf) {
		this.Fname_tf = Fname_tf;
	}
	
	public String getLname() {
		return Lname_tf;
	}

	public void setLname(String Lname_tf) {
		this.Lname_tf = Lname_tf;
	}
	
	public String getParent_First() {
		return Parent_first_name_tf;
	}

	public void setParent_First(String Parent_first_name_tf) {
		this.Parent_first_name_tf = Parent_first_name_tf;
	}
	
	public String getParent_Last() {
		return Parent_last_name_tf;
	}

	public void setParents_Last(String Parent_last_name_tf) {
		this.Parent_last_name_tf = Parent_last_name_tf;
	}
	
	public String getDob_dc() {
		return Dob_dc;
	}

	public void setDob_dc(String Dob_dc) {
		this.Dob_dc = Dob_dc;
	}

	public String getDate_of_admission_dc() {
		return Date_of_admission_dc;
	}

	public void setDate_of_admission_dc(String Date_of_admission_dc) {
		this.Date_of_admission_dc = Date_of_admission_dc;
	}

	public String getGender_group () {
		return Gender_group ;
	}

	public void setGender_group (String Gender_group ) {
		this.Gender_group  = Gender_group ;
	}


	public String getEmail() {
		return Email_tf;
	}

	public void setEmail(String Email_tf) {
		this.Email_tf = Email_tf;
	}

	public String getphone_no() {
		return phone_no_tf;
	}

	public void setphone_no(String phone_no_tf) {
		this.phone_no_tf = phone_no_tf;
	}

	public String getStreet_no() {
		return Street_no_tf;
	}

	public void setStreet_no(String Street_no_tf) {
		this.Street_no_tf = Street_no_tf;
	}

	public String getHouse_no() {
		return House_no_tf;
	}

	public void setHouse_no(String House_no_tf) {
		this.House_no_tf = House_no_tf;
	}

	public String getZip_code() {
		return Zip_code_tf;
	}

	public void setZip_code(String Zip_code_tf) {
		this.Zip_code_tf = Zip_code_tf;
	}
	
	
		
	public String getClass_to_attend_cb() {
		return Class_to_attend_cb;
	}

	public void setClass_to_attend_cb(String Class_to_attend_cb) {
		this.Class_to_attend_cb = Class_to_attend_cb;
	}
	
	public String getroom_cb() {
		return room_cb;
	}

	public void setroom_cb(String room_cb) {
		this.room_cb = room_cb;
	}


}

