package VLC;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.Container;
import java.awt.event.ActionListener;
import java.util.Map;
import java.awt.event.*; import javax.swing.*;

import javax.swing.*;

public class Menu {
	JFrame frame;
	JLabel Menu;
	JButton Studreg_btn;
    JButton Coachreg_btn;
    JButton Appointment_btn;
    JButton Record_btn;
    JButton Attend_btn;
    JButton Exit_btn;
    Container c;
    JLabel Logo;
    
    Menu(){
    	frame=new JFrame("Venue Liesure Center");
    	
    	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);   
        
        frame.setSize(345, 400); 
        
        frame.setLocationRelativeTo(null);
          
        frame.getContentPane().setLayout(null);   
          
        frame.setVisible(true);
        
        Menu = new JLabel("Menu"); 
        
        Menu.setBounds(139, 11, 47, 26); 
                 
        frame.getContentPane().add(Menu); 
        

        Studreg_btn = new JButton("Student Registration"); 
        Studreg_btn.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		studentreg.main(null);
 		       frame.dispose();
        	}
        });
        
        Studreg_btn.setBounds(87, 72, 155, 22);
                 
        frame.getContentPane().add(Studreg_btn);
        

        Coachreg_btn = new JButton("Coach Registration"); 
        Coachreg_btn.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        	   Coachreg.main(null);
  		       frame.dispose();
        	}
        });
        
        Coachreg_btn.setBounds(87, 123, 155, 22);
                 
        frame.getContentPane().add(Coachreg_btn);

        
        Appointment_btn = new JButton("Appointment"); 
        Appointment_btn.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		Appointment.main(null);
  		       frame.dispose();
        	}
        });
        
        Appointment_btn.setBounds(87, 172, 155, 22);
                 
        frame.getContentPane().add(Appointment_btn);
        
        
        Record_btn = new JButton("Records"); 
        Record_btn.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        	   Records.main(null);
  		       frame.dispose();
        	}
        });
        
        Record_btn.setBounds(87, 214, 155, 22);
                 
        frame.getContentPane().add(Record_btn);
        
        Attend_btn = new JButton("Attendence"); 
        Attend_btn.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        	   Attendence.main(null);
  		       frame.dispose();
        	}
        });
        
        Attend_btn.setBounds(87, 257, 155, 22);
                 
        frame.getContentPane().add(Attend_btn);
        
        
        Exit_btn = new JButton("Exit"); 
        Exit_btn.setBounds(87, 316, 155, 22);
                 
        frame.getContentPane().add(Exit_btn);
        
        Exit_btn = new JButton("Exit"); 
        Exit_btn.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent arg0) {
          int result = JOptionPane.showConfirmDialog(frame, "Are you sure you want to Exit the system?","Exit System",JOptionPane.YES_NO_OPTION);
          if(result == JOptionPane.YES_OPTION)//Return value from class method if YES is chosen 

               {
                System.exit(0);
               }
}
});
               
       
        
    }
    
    public static void main(String[] args) { 
        new Menu(); 
        
    } 

}
