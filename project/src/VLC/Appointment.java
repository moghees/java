package VLC;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.Container;
import java.util.ArrayList;
import java.util.Map;
import java.awt.event.*; import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import com.toedter.calendar.JDateChooser;
import lu.tudor.santec.jtimechooser.JTimeChooser;

import javax.swing.*;

public class Appointment {
	JFrame frame; 
    JLabel Appointment;
    JLabel Coach_id;
    JTextField Coachid_tf;
    JLabel Fname;
    JTextField Fname_tf;
    JLabel Lname;
    JTextField Lname_tf;
    JLabel Gender;
    JRadioButton Male_rbtn;
    JRadioButton Female_rbtn;
    JRadioButton Other_rbtn;
    ButtonGroup Gender_group;
    JLabel Number;
    JTextField Number_tf;
    JLabel Appointment_d;
    JDateChooser Appointment_date_dc;
    JLabel Appointment_t;
    JTimeChooser Appointment_time_tc;
    JLabel A_room;
    JComboBox A_room_cb;
    JButton Save_btn;
    JButton Back_btn;
    JButton Clear_btn;
    JTable Table;
    JLabel Logo;
    JButton Load_btn;
    DefaultTableModel modell;
    JTable tab;
    JScrollPane scroll;
    private JLabel label;
    
    Appointment(){
    	frame=new JFrame("Venue Liesure Center");
    	
    	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
        
        frame.setSize(688, 604); 
        
        frame.setLocationRelativeTo(null);
          
        frame.getContentPane().setLayout(null);   
          
        frame.setVisible(true);
        
        Appointment = new JLabel("Appointment"); 
        
        Appointment.setBounds(256, 11, 126, 26); 
                 
        frame.getContentPane().add(Appointment);
        
        
    	Coach_id = new JLabel("Coach ID"); 
        
		Coach_id.setBounds(39, 58, 95, 16); 
  
		frame.getContentPane().add(Coach_id);

    	Coachid_tf = new JTextField(); 

    	Coachid_tf.setBounds(144, 55, 210, 22);
  
    	frame.getContentPane().add(Coachid_tf);
    	
    	
    	 Fname = new JLabel("First Name"); 
         
         Fname.setBounds(39, 95, 95, 16); 
           
         frame.getContentPane().add(Fname);
         
  	   Fname_tf = new JTextField(); 
         
         Fname_tf.setBounds(144, 92, 210, 22);
           
         frame.getContentPane().add(Fname_tf);
         
         
         
         Lname = new JLabel("Last Name"); 
         
         Lname.setBounds(39, 122, 98, 20); 
                  
         frame.getContentPane().add(Lname);
                
         Lname_tf = new JTextField(); 
                
         Lname_tf.setBounds(144, 120, 210, 22);
                  
         frame.getContentPane().add(Lname_tf);
         
         
         Gender = new JLabel("Gender"); 
	        
         Gender.setBounds(39, 147, 51, 26); 
  	         
         frame.getContentPane().add(Gender);
  	
         Male_rbtn = new JRadioButton("Male");

         Male_rbtn.setBounds(143, 149, 64, 23);
              
         frame.getContentPane().add(Male_rbtn);

         Female_rbtn = new JRadioButton("Female");

         Female_rbtn.setBounds(220, 149, 75, 23);
               
         frame.getContentPane().add(Female_rbtn);
                
         Other_rbtn = new JRadioButton("Other");

         Other_rbtn.setBounds(308, 149, 63, 23);
                
         frame.getContentPane().add(Other_rbtn);
  
         Gender_group = new ButtonGroup();
         Gender_group.add(Male_rbtn);
         Gender_group.add(Female_rbtn);
         Gender_group.add(Other_rbtn);
        
 		  
 		   Number = new JLabel("Number"); 
          
          Number.setBounds(39, 182, 98, 20); 
                   
          frame.getContentPane().add(Number);
                 
          Number_tf = new JTextField(); 
                 
          Number_tf.setBounds(144, 181, 210, 22);
                   
          frame.getContentPane().add(Number_tf);
 		   
 		
          
		   Appointment_d = new JLabel("Appoinment Date"); 
	          
	       Appointment_d.setBounds(39, 262, 98, 20); 
	                   
	       frame.getContentPane().add(Appointment_d);

        Appointment_date_dc = new JDateChooser();
        
	      Appointment_date_dc.setBounds(144, 262, 210, 20);
	      
	     
    	  frame.getContentPane().add(Appointment_date_dc);
    	  

    	  Appointment_t = new JLabel("Appoinment Time"); 
          
	       Appointment_t.setBounds(39, 330, 98, 20); 
	                   
	       frame.getContentPane().add(Appointment_t);
    	  Appointment_time_tc = new JTimeChooser();
	        
          Appointment_time_tc.setBounds(161, 324, 126, 26);
          
      	  frame.getContentPane().add(Appointment_time_tc);
      	  
      	  
      	   A_room = new JLabel("Select Room");
	        
           A_room.setBounds(39, 213, 97, 26);

           frame.getContentPane().add(A_room);

           A_room_cb = new JComboBox();

           A_room_cb.setModel(new DefaultComboBoxModel(new String[] {"Studio A", "Studio B", "Studio C", "Pool", "Gym"}));

           A_room_cb.setBounds(144, 214, 87, 28);

           frame.getContentPane().add(A_room_cb);
        
    	  
        
        Save_btn = new JButton("Set Appointment"); 
        Save_btn.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		 ArrayList<Appointmentdata> al = Singleton.getInstance(). getAppointmentdataList();
    		 Appointmentdata fbd = new Appointmentdata(Coachid_tf.getText(), Fname_tf.getText(), Lname_tf.getText(), Gender_group.getSelection().toString(), Number_tf.getText(),A_room_cb.getSelectedItem().toString(), Appointment_date_dc.getDate().toString(), Appointment_time_tc.getTimeField().getText());
        	 
	                	 al.add(fbd);
	                	JOptionPane.showMessageDialog(null, "Data Entered Successfully");  
        	}
        });
        
        Save_btn.setBounds(536, 91, 126, 22);
                 
        frame.getContentPane().add(Save_btn);

        
        Load_btn = new JButton("Load Appointment"); 
        Load_btn.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		ArrayList<Appointmentdata> vector = Singleton.getInstance(). getAppointmentdataList();
  		            String[] columnNames = {"Coach id", "First Name", "Last Name", "Number", "Gender", "Room", "Appointment date", "Appointment time"}; 

  		  	 Object[][] data;
  		  	 
			data = new Object[vector.size()][10];

			for (int i = 0; i < data.length; i++) {
				Appointmentdata a = vector.get(i);
				
				data[i][0] = a.getCoachid_tf();
				data[i][1] = a.getFname_tf();
				data[i][2] = a.getLname_tf();
				data[i][3] = a.getNumber();
				data[i][4] = a.getGender_group();
				data[i][5] = a.getA_room_cb();
				data[i][6] = a.getAppointment_date_dc();
				data[i][7] = a.getAppointment_time_tc();
			}
  				modell = new DefaultTableModel(data, columnNames);
  			tab = new JTable(modell);
  		scroll  = new JScrollPane(tab);

  		scroll.setVisible(true);

  		scroll.setBounds(10, 371, 652, 183);

                    frame.getContentPane().add(scroll);	
                 
        	}
        });
        
        Load_btn.setBounds(536, 58, 126, 22);
                 
        frame.getContentPane().add(Load_btn);
               
        
        Back_btn = new JButton("Back"); 
        Back_btn.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		Menu.main(null);
        		frame.dispose();
        	}
        });
               
        Back_btn.setBounds(536, 124, 126, 22);
                        
        frame.getContentPane().add(Back_btn);
        
        
       
        Clear_btn = new JButton("Clear"); 
        Clear_btn.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		
        	    Coachid_tf.setText("");
        	    
        	    Fname_tf.setText("");
        	    
        	    Lname_tf.setText("");
        	    
        	    Number_tf.setText("");
        	    
        	    Gender_group.clearSelection();
        	    
        	    A_room_cb.setSelectedIndex(0);
        	    
        	    Appointment_date_dc.setDate(null);

        	    Appointment_time_tc.getTimeField().setValue("00:00:00:");
        	    
        		}
        });
        Clear_btn.setBounds(536, 172, 110, 22);                
        frame.getContentPane().add(Clear_btn);
        

       
    	
    }
    
    public static void main(String[] args) { 
        new Appointment(); 
        
        Appointmentdata fbd1 = new Appointmentdata("Coach id", "First Name", "Last Name", "Number", "Gender", "Room", "Appointment date", "Appointment time"); 
        Appointmentdata fbd2 = new Appointmentdata("Coach id", "First Name", "Last Name", "Number", "Gender", "Room", "Appointment date", "Appointment time"); 
        Appointmentdata fbd3 = new Appointmentdata("Coach id", "First Name", "Last Name", "Number", "Gender", "Room", "Appointment date", "Appointment time"); 
        Appointmentdata fbd4 = new Appointmentdata("Coach id", "First Name", "Last Name", "Number", "Gender", "Room", "Appointment date", "Appointment time"); 
        Appointmentdata fbd5 = new Appointmentdata("Coach id", "First Name", "Last Name", "Number", "Gender", "Room", "Appointment date", "Appointment time"); 
        
        
        ArrayList <Appointmentdata> list =  Singleton.getInstance().getAppointmentdataList();			
	    
	    list.add(fbd1);
	    list.add(fbd2);
	    list.add(fbd3);
	    list.add(fbd4);
	    list.add(fbd5);
        
    } 

}
