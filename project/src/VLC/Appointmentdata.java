package VLC;

public class Appointmentdata {

	private String Coachid_tf, Fname_tf, Lname_tf,Gender_group, Number_tf, A_room_cb, Appointment_date_dc, Appointment_time_tc;


	public Appointmentdata(String Coachid_tf,String Fname_tf,String Lname_tf,String Gender_group, String Number_tf ,String A_room_cb, String Appointment_date_dc, String Appointment_time_tc ) {
		super();
	
		this.Coachid_tf = Coachid_tf;
		this.Fname_tf = Fname_tf;
		this.Lname_tf = Lname_tf;
		this.Gender_group = Gender_group;
		this.Number_tf = Number_tf;
		this.A_room_cb = A_room_cb;
		this.Appointment_date_dc = Appointment_date_dc;
		this.Appointment_time_tc = Appointment_time_tc;
		
	
	}

	public String getCoachid_tf() {
		return Coachid_tf;
	}

	public void setCoachid_tf(String Coachid_tf) {
		this.Coachid_tf = Coachid_tf;
	}
	
	public String getFname_tf() {
		return Fname_tf;
	}

	public void setFname_tf(String Fname_tf) {
		this.Fname_tf = Fname_tf;
	}
	
	public String getLname_tf() {
		return Lname_tf;
	}

	public void setLname_tf(String Lname_tf) {
		this.Lname_tf = Lname_tf;
	}
	
	public String getNumber() {
		return Number_tf;
	}
	public void setNumber(String Number_tf) {
		this.Number_tf = Number_tf;
	}
	
	public String getGender_group() {
		return Gender_group;
	}
	public void setGender_group(String Gender_group) {
		this.Gender_group = Gender_group;
	}
	
	public String getA_room_cb() {
		return A_room_cb;
	}

	public void setA_room_cb(String A_room_cb) {
		this.A_room_cb = A_room_cb;
	}
	
	public String getAppointment_date_dc() {
		return Appointment_date_dc;
	}

	public void setAppointment_date_dc(String Appointment_date_dc) {
		this.Appointment_date_dc = Appointment_date_dc;
	}
	
	public String getAppointment_time_tc() {
		return  Appointment_time_tc;
	}

	public void setAppointment_time_tc(String  Appointment_time_tc) {
		this. Appointment_time_tc =  Appointment_time_tc;
	}


}
