package VLC;

public class Attendencedata {

	private String Coachid_tf, Student_name_tf, Class_attend_cb, Appointment_date_dc, Appointment_time_tc;


	public Attendencedata(String Coachid_tf,String Student_name_tf, String Class_attend_cb, String Appointment_date_dc, String Appointment_time_tc ) {
		super();
	
		this.Coachid_tf = Coachid_tf;
		this.Student_name_tf = Student_name_tf;
		this.Class_attend_cb = Class_attend_cb;
		this.Appointment_date_dc = Appointment_date_dc;
		this.Appointment_time_tc = Appointment_time_tc;
		
	
	}

	public String getCoachid_tf() {
		return Coachid_tf;
	}

	public void setCoachid_tf(String Coachid_tf) {
		this.Coachid_tf = Coachid_tf;
	}
	
	public String getStudent_name_tf() {
		return Student_name_tf;
	}

	public void setFname(String Student_name_tf) {
		this.Student_name_tf = Student_name_tf;
	}
	

	public String getClass_attend_cb() {
		return Class_attend_cb;
	}

	public void setA_room_cb(String Class_attend_cb) {
		this.Class_attend_cb = Class_attend_cb;
	}
	
	public String getAppointment_date_dc() {
		return Appointment_date_dc;
	}

	public void setAppointment_date_dc(String Appointmen_date_dc) {
		this.Appointment_date_dc = Appointment_date_dc;
	}
	
	public String getAppointment_time_tc() {
		return  Appointment_time_tc;
	}

	public void setAppointment_time_tc(String  Appointment_time_tc) {
		this. Appointment_time_tc =  Appointment_time_tc;
	}


}
