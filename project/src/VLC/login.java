package VLC;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.Container;
import java.awt.event.ActionListener;
import java.util.Map;
import java.awt.event.*; import javax.swing.*;

import javax.swing.*;


public class login implements ActionListener{ 
	
    JFrame frame; 
    JLabel Vlc;
    JLabel Username;    JTextField Username_tf;
    JLabel Password;    JPasswordField Password_pf;
    JButton Login_btn;
    Container c;
    JLabel Logo;
    login() 
    { 
         
        frame=new JFrame("Venue Leisure Centre"); 
        Image img = Toolkit.getDefaultToolkit().getImage("Resources/img/vlcmin.PNG");
        frame.setIconImage(img);
        c = frame.getContentPane();
		c.setLayout(new BorderLayout());
    	JPanel centerpanel = new JPanel(new GridLayout(4, 3));
    	JPanel northpanel = new JPanel(new FlowLayout());
    	JPanel southpanel = new JPanel(new FlowLayout());
    	JPanel eastpanel = new JPanel(new FlowLayout());
    	JPanel westpanel = new JPanel(new FlowLayout());

    	Vlc = new JLabel("Venue Leisure Centre");
    	Username = new JLabel("Username");
    	Username_tf = new JTextField();
    	Password = new JLabel("Password"); 
    	Password_pf = new JPasswordField(); 
    	Login_btn = new JButton("Login"); 
    	Logo = new JLabel(new ImageIcon("res/img/VLC-icon.png"));
    	  
    	
        northpanel.add(Vlc);
        
		centerpanel.add(Username);       
        centerpanel.add(Username_tf);     
        centerpanel.add(Password); 
        centerpanel.add(Password_pf); 
        southpanel.add( Login_btn); 
        eastpanel.add(Logo); 
        
        Login_btn.addActionListener(this);
        
        c.add(northpanel, BorderLayout.NORTH);
        c.add(centerpanel, BorderLayout.CENTER);
        c.add(southpanel, BorderLayout.SOUTH);
        c.add(eastpanel, BorderLayout.EAST);
        c.add(westpanel, BorderLayout.WEST);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
        
        
        frame.setSize(705, 301); 
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);  
          
        frame.setVisible(true);
           
    } 
 public void actionPerformed(ActionEvent e){
    	
    	if(e.getActionCommand()=="Login" && !Username_tf.getText().isEmpty() && !new String(Password_pf.getPassword()).isEmpty())
    	{
        	Map<String, Admin> adminmap = Singleton.getInstance().getAdminUserList();
        	Admin admin = adminmap.get(Username_tf.getText());
        	String passText = new String(Password_pf.getPassword());
        	if(admin!=null && passText.equals(admin.getPassword())){
        		frame.dispose();
        		new Menu();
        	}  
        	else{
        		JOptionPane.showMessageDialog(null, "Username or Password is incorrect");
        	}
    	}
    	else{
    		JOptionPane.showMessageDialog(null, "Enter username and password");

    	}
    }
 public static void main(String[] args) { 
 	Admin admin1 = new Admin ("moghees","1234");
 	Admin admin2 = new Admin ("admin2","adminpassword2");
 	Admin admin3 = new Admin ("admin3","adminpassword3");
 	Map<String, Admin> adminmap = Singleton.getInstance().getAdminUserList();
 	
 	adminmap.put("moghees",  admin1);
 	adminmap.put("admin2",  admin2);
 	adminmap.put("admin3",  admin3);
 	
 	
     new login(); 
     
 } 
} 