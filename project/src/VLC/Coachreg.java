package VLC;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import com.toedter.calendar.JDateChooser;

import javax.swing.border.Border;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.*;
import java.io.File;
import java.util.ArrayList;

public class Coachreg {
	 JFrame frame;
	 JMenuBar menubar1;
	 JMenu filemenu;  JMenuItem Exit;
	 JMenu openmenu;  
	 JMenuItem Records_mi; 
	 JMenuItem studentreg_mi;  
	 JMenuItem Appointment_mi; 
	 JMenuItem Attendence_mi;
	 JMenu Logoutmenu; JMenuItem Logout_mi;
	 JLabel Coach_registration; 
	 JLabel Coach_id;                  
	 JTextField Coach_id_tf;         
	 JLabel Fname;                  
	 JTextField Fname_tf;  
	 JLabel Lname;                   
	 JTextField Lname_tf;  
	 JLabel Parent_first_name;        
	 JTextField Parent_first_name_tf;  	        
	 JLabel Parent_last_name;         
	 JTextField Parent_last_name_tf; 
	 JLabel Dob;               
	 JDateChooser Dob_dc;
	 JLabel Admission_date;           
	 JDateChooser Admission_date_dc;
	 JLabel Gender;                      
	 JRadioButton Male_rbtn;             
	 JRadioButton Female_rbtn;     
	 JRadioButton Other_rbtn;
	 ButtonGroup Gender_group;
	 JLabel Email;                       
	 JTextField Email_tf;
	 JLabel phone_no;                
	 JTextField phone_no_tf; 
	 JLabel House_no;                    
	 JTextField House_no_tf;       
	 JLabel Street_no;                   
	 JTextField Street_no_tf;  	        
	 JLabel Zip_code;                    
	 JTextField Zip_code_tf;          
	 JLabel Class_to_attend;             
	 JComboBox Class_to_attend_cb;
	 JLabel room;                
	 JComboBox room_cb; 
     JButton Load_data_btn;
     JTable Coach_registration_table;   
     JScrollPane Coach_registration_table_sp;
     JButton Save_btn; 
	 JButton Back_btn;    	        
	 JButton Clear_btn; 
	 JButton Exit_btn; 
	 DefaultTableModel modell;
	 
	 
	 JLabel Logo;
	 

	  
	 Coachreg(){ 
	         
	        frame=new JFrame("Venue Leisure Centre");
	        Image img = Toolkit.getDefaultToolkit().getImage("res/img/VLC-icon.PNG");
	        frame.setIconImage(img);
	        
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
	        
	        frame.setSize(998, 788); 
	        
	        frame.setLocationRelativeTo(null);
	          
	        frame.getContentPane().setLayout(null);   
	          
	        frame.setVisible(true);
	        
	        
	        menubar1 = new JMenuBar ();
	        frame.setJMenuBar (menubar1);
	        
	        filemenu = new JMenu ("file");
	        menubar1.add (filemenu);
	        
	       
	        
	        openmenu = new JMenu ("Open");
	        filemenu.add (openmenu);
	     
	        
	        
	        openmenu.add(new JSeparator());
	        
	        	Records_mi = new JMenuItem("Records");
	        	Records_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        	Records.main(null);
	  		    frame.dispose();
	        	}
	        });
	        	
	        openmenu.add(Records_mi);
	        Records_mi.setIcon(new ImageIcon(this.getClass().getResource("/img/records.png")));
	        
	        openmenu.add(new JSeparator());
	        
	        Appointment_mi = new JMenuItem("Appointment");
	        Appointment_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		 Appointment.main(null);
	  		       frame.dispose();
	        	}
	        });
	        
	        openmenu.add(Appointment_mi);
	        Appointment_mi.setIcon(new ImageIcon(this.getClass().getResource("/img/appointment.png")));
	        
	        openmenu.add(new JSeparator());
	        
	        Appointment_mi = new JMenuItem("Appointment");
	        Appointment_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		 Appointment.main(null);
	  		       frame.dispose();
	        	}
	        });


	        
 openmenu.add(new JSeparator());
	        
	        studentreg_mi = new JMenuItem("Student Registration");
	        studentreg_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        	   studentreg.main(null);
	  		       frame.dispose();
	        	}
	        });
	        openmenu.add(studentreg_mi);
	        studentreg_mi.setIcon(new ImageIcon(this.getClass().getResource("/img/student.png")));
	        
	        
	        
	        filemenu.add(new JSeparator());
	        
	        Exit = new JMenuItem("Exit");
	        Exit.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent arg0) {
	        		 int result = JOptionPane.showConfirmDialog(frame, "Do you want to Exit?","Exit System",JOptionPane.YES_NO_OPTION);
		             if(result == JOptionPane.YES_OPTION)//Return value from class method if YES is chosen 

		                    {
		                     System.exit(0);
		                    }
	        	}
	        });
	        filemenu.add(Exit);
	      
	        
	        Logoutmenu = new JMenu ("Logout");
	        menubar1.add (Logoutmenu);
	         
	        
	        Logout_mi = new JMenuItem("Logout");
	        Logout_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent arg0) {
	        		int result = JOptionPane.showConfirmDialog(frame, "Do you want to logout?","Logout",JOptionPane.YES_NO_OPTION);
		             if(result == JOptionPane.YES_OPTION)//Return value from class method if YES is chosen 
		                    {
		            	     login.main(null);
			                 frame.dispose();
		                    }
	        	}
	        });
	        Logoutmenu.add(Logout_mi);
	        Logout_mi.setIcon(new ImageIcon(this.getClass().getResource("/img/logout.png"))); 
	        
	        
	        
	          
            Coach_registration = new JLabel("Coach Registration"); 
	        
            Coach_registration.setBounds(426, 13, 126, 26); 
	          
	        frame.getContentPane().add(Coach_registration); 
	        
            Coach_id = new JLabel("Coach ID"); 
	        
            Coach_id.setBounds(24, 55, 75, 26); 
	        	          
	        frame.getContentPane().add(Coach_id);
	        	
	        Coach_id_tf = new JTextField(); 
	        	        
	        Coach_id_tf.setBounds(163, 57, 210, 22);
	        	          
	        frame.getContentPane().add(Coach_id_tf);
	        
            Fname = new JLabel("First Name"); 
	        Fname.setBounds(24, 94, 75, 26); 
	          
	        frame.getContentPane().add(Fname);
	                          
	        Fname_tf = new JTextField(); 
	        
            Fname_tf.setBounds(163, 98, 210, 22);
	          
	                 frame.getContentPane().add(Fname_tf);

                     Lname = new JLabel("Last Name"); 
	        
                     Lname.setBounds(24, 133, 75, 26); 
	          
	                 frame.getContentPane().add(Lname);
	
	                 Lname_tf = new JTextField(); 
	        
                     Lname_tf.setBounds(163, 137, 210, 22);
	          
	                 frame.getContentPane().add(Lname_tf);
	        
                     Parent_first_name = new JLabel("Parents first Name");
	                 
	                 Parent_first_name.setBounds(24, 174, 129, 26);
	                 frame.getContentPane().add(Parent_first_name);
	
	                 Parent_first_name_tf = new JTextField(); 
	        
                     Parent_first_name_tf.setBounds(163, 176, 210, 22);
	          
	                 frame.getContentPane().add(Parent_first_name_tf);
	        
                     Parent_last_name = new JLabel("Parent's Last Name"); 
	        
                     Parent_last_name.setBounds(24, 207, 126, 26); 
	          
	                 frame.getContentPane().add(Parent_last_name);
	
	                 Parent_last_name_tf = new JTextField(); 
	        
                     Parent_last_name_tf.setBounds(163, 209, 210, 22);
	          
	                 frame.getContentPane().add(Parent_last_name_tf);

	                 Dob = new JLabel("Date Of Birth"); 
	        
                     Dob.setBounds(24, 246, 87, 26); 
	          
	                 frame.getContentPane().add(Dob);
	        
	                 Dob_dc = new JDateChooser();
	        
	                 Dob_dc.setBounds(163, 252, 210, 20);
                  
	        	    frame.getContentPane().add(Dob_dc);
	        	        	        	      
	        	    Admission_date = new JLabel("Admission Date");
	        	        	      	        
	        	    Admission_date.setBounds(24, 285, 137, 26);
	        		        	        
	        		frame.getContentPane().add( Admission_date);
	        		                                              
	        		Admission_date_dc = new JDateChooser();
	        		                                              
	        		Admission_date_dc.setBounds(163, 288, 210, 20);

	        		frame.getContentPane().add(Admission_date_dc);
	        
	        		Gender = new JLabel("Gender"); 
	        
                    Gender.setBounds(24, 324, 51, 26); 
	        	          
	                frame.getContentPane().add(Gender);
	        	
	                Male_rbtn = new JRadioButton("Male");

                    Male_rbtn.setBounds(163, 326, 64, 23);
	                    
	                frame.getContentPane().add(Male_rbtn);

                    Female_rbtn = new JRadioButton("Female");

                    Female_rbtn.setBounds(231, 326, 75, 23);
	                     
	                frame.getContentPane().add(Female_rbtn);
	                      
            Other_rbtn = new JRadioButton("Other");

            Other_rbtn.setBounds(310, 326, 63, 23);
	                      
	        frame.getContentPane().add(Other_rbtn);
	        
	        Gender_group = new ButtonGroup();
	        Gender_group.add(Male_rbtn);
	        Gender_group.add(Female_rbtn);
	        Gender_group.add(Other_rbtn);
	        

	        
            Email = new JLabel("Email"); 
	        
            Email.setBounds(426, 55, 75, 26); 
	        	          
            frame.getContentPane().add(Email);
	        	
            Email_tf = new JTextField(); 
	        	        
            Email_tf.setBounds(524, 59, 210, 22);
	        
	        frame.getContentPane().add(Email_tf);
	        	        
            phone_no = new JLabel("Phone No");
	        	        
            phone_no.setBounds(427, 94, 87, 26);
	                    	        
	        frame.getContentPane().add(phone_no);
	                                
            phone_no_tf = new JTextField(); 
	                    	        
            phone_no_tf.setBounds(524, 96, 210, 22);
	                    	          
	        frame.getContentPane().add(phone_no_tf);
	                    	        
	        Street_no = new JLabel("Street No");
	                                     	        
	        Street_no.setBounds(426, 174, 75, 26);
	                            	        
	        frame.getContentPane().add(Street_no);
	                                        
	        Street_no_tf = new JTextField(); 
	                            	        
	        Street_no_tf.setBounds(524, 173, 210, 22);
	                            	          
	        frame.getContentPane().add(Street_no_tf);
	                            	        
	                                                 
            House_no = new JLabel("House No");
	        
            House_no.setBounds(426, 207, 75, 26);
	        
            frame.getContentPane().add(House_no);
            
            House_no_tf = new JTextField(); 
	        
            House_no_tf.setBounds(524, 212, 210, 22);
	          
	        frame.getContentPane().add(House_no_tf);
	        
            
            Zip_code = new JLabel("Zip Code");
	        
            Zip_code.setBounds(426, 246, 75, 26);
	        
            frame.getContentPane().add(Zip_code);
            
            Zip_code_tf = new JTextField(); 
	        
            Zip_code_tf.setBounds(524, 251, 210, 22);
	          
	        frame.getContentPane().add(Zip_code_tf);
	        
	        
            Class_to_attend = new JLabel("Class to Attend");
	        
            Class_to_attend.setBounds(426, 285, 97, 26);
	        
            frame.getContentPane().add(Class_to_attend);
            
            Class_to_attend_cb = new JComboBox();
	        
            Class_to_attend_cb.setModel(new DefaultComboBoxModel(new String[] {"Swimming", "badminton", "gym"}));
	        
            Class_to_attend_cb.setBounds(524, 288, 87, 26);
			
	        frame.getContentPane().add(Class_to_attend_cb);
	        
           
	        room = new JLabel("Room");
	        
            room.setBounds(426, 324, 97, 26);

            frame.getContentPane().add(room);

            room_cb = new JComboBox();

            room_cb.setModel(new DefaultComboBoxModel(new String[] {"Studio A", "Studio B", "Studio C", "Pool", "Gym"}));

            room_cb.setBounds(524, 326, 87, 28);

            frame.getContentPane().add(room_cb);
                  
                  Border border = BorderFactory.createLineBorder(Color.BLACK, 2);
                                          
                     
                     Load_data_btn = new JButton("Load Data"); 
                     Load_data_btn.addActionListener(new ActionListener() {
                     	public void actionPerformed(ActionEvent arg0) {
                     		
                     		ArrayList<Coachdata> vector = Singleton.getInstance().getCoachdataList();
   		   		            String[] columnNames = {"Coach Id", "First Name", "Last Name", "Parents First Name", "Parents Last Name", "Date of Birth", "Date of Admission", "Gender", "Email", "Phone No", "Street No", "House No", "Zip Code", "Class to Attend", "Room"}; 

   		   		  	 Object[][] data;
   		   		  	 
   					data = new Object[vector.size()][15];

   					for (int i = 0; i < data.length; i++) {
   						Coachdata a = vector.get(i);
   						data[i][0] = a.getCoach_id();
   						data[i][1] = a.getFname();
   						data[i][2] = a.getLname();
   						data[i][3] = a.getParent_First();
   						data[i][4] = a.getParent_Last();
   						data[i][5] = a.getDob_dc();
   						data[i][6] = a.getDate_of_admission_dc();
   						data[i][7] = a.getGender_group();
   						data[i][8] = a.getEmail();
   						data[i][9] = a.getphone_no();
   						data[i][10] = a.getStreet_no();
   						data[i][11] = a.getHouse_no();
   						data[i][12] = a.getZip_code();
   						data[i][13] = a.getClass_to_attend_cb();
   						data[i][14] = a.getroom_cb();
   						
   						
   					}
   		   				modell = new DefaultTableModel(data, columnNames);
   		   			Coach_registration_table = new JTable(modell);
   		   			Coach_registration_table_sp  = new JScrollPane(Coach_registration_table);

   		   			Coach_registration_table_sp.setVisible(true);

   		   			Coach_registration_table_sp.setBounds(24, 402, 893, 420);

	   	            frame.getContentPane().add(Coach_registration_table_sp);	
   		                  
   		                 }
   		                 
                   });	
                     
                       
	                    
                     Load_data_btn.setBounds(817, 52, 124, 33);
	        	        	        	                    	   	          
	                                  frame.getContentPane().add(Load_data_btn);
	                                  
	                                  Save_btn = new JButton("Save"); 
	                                  Save_btn.addActionListener(new ActionListener() {
	                                  	public void actionPerformed(ActionEvent e) {
	                                  		 ArrayList<Coachdata> al = Singleton.getInstance().getCoachdataList();
	                                  		Coachdata fbd = new Coachdata(Coach_id_tf.getText(), Fname_tf.getText(), Lname_tf.getText(), Parent_first_name_tf.getText(), Parent_last_name_tf.getText(), Dob_dc.getDate().toString(), Admission_date_dc.getDate().toString(), Gender_group.getSelection().toString(), Email_tf.getText(), phone_no_tf.getText(), Street_no_tf.getText(),House_no_tf.getText(), Zip_code_tf.getText(), Class_to_attend_cb.getSelectedItem().toString(), room_cb.getSelectedItem().toString());
	       		                	 
	     		   		                	 al.add(fbd);
	     		   		                	JOptionPane.showMessageDialog(null, "Data entered successfully");    
	     		   		                 }
	     		   	                });	
	                                  
	                                  
	                             
	          	   			        
	                                  Save_btn.setBounds(817, 91, 124, 33);
	              	   			          
	              	   	                     frame.getContentPane().add(Save_btn);	
	        
            Back_btn = new JButton("Back"); 
            Back_btn.addActionListener(new ActionListener() {
	           public void actionPerformed(ActionEvent e) {
		       Menu.main(null);
		       frame.dispose();
	}
});
	   	        
           
            Back_btn.setBounds(817, 132, 124, 33);
	   	       
	   	             frame.getContentPane().add(Back_btn);
	   	        
            Clear_btn = new JButton("Clear");
            Clear_btn.addActionListener(new ActionListener() {
	          public void actionPerformed(ActionEvent e) {
		      Coach_id_tf.setText("");         
		  
		      Fname_tf.setText("");  
		 
		      Lname_tf.setText("");  
		  
		      Parent_first_name_tf.setText("");  	        
		
		      Parent_last_name_tf.setText(""); 
		
		      Dob_dc.setDate(null);
		      
		 	  Admission_date_dc.setDate(null);
		      
		      Gender_group.clearSelection();
		
		      Email_tf.setText("");
		           
		      phone_no_tf.setText(""); 
		           
		      House_no_tf.setText("");       
		 
		      Street_no_tf.setText("");  	        
		     
		      Zip_code_tf.setText("");          
		           
		      Class_to_attend_cb.setSelectedIndex(0);
		      
		      room_cb.setSelectedIndex(0); 
		          
	}
});

              
            
              Clear_btn.setBounds(817, 171, 124, 33);
	   		          
	   		            frame.getContentPane().add(Clear_btn);
	   		
	   		        
                   

            Exit_btn = new JButton("Exit"); 
                    Exit_btn.addActionListener(new ActionListener() {
	                    public void actionPerformed(ActionEvent arg0) {
		                     int result = JOptionPane.showConfirmDialog(frame, "Do you want to Exit?","Exit System",JOptionPane.YES_NO_OPTION);
		                     if(result == JOptionPane.YES_OPTION)//Return value from class method if YES is chosen 

		                   {
		                     System.exit(0);
		                   }
	}
});
            
                    
            Exit_btn.setBounds(817, 212, 124, 33);
            
               
	   				          
	   		         frame.getContentPane().add(Exit_btn);
	   		
                                                                          
                                                                          Logo = new JLabel();
                                                                         
                                                              	        
                                                                          Logo.setBounds(925, 747, 202, 82);
                                                      	        
                                                                                  frame.getContentPane().add(Logo);                                                       
			
	    } 
	      
	    public static void main(String[] args) { 
	        new Coachreg(); 
	        
	        Coachdata    Coachreg1 = new Coachdata ("Coach Id", "First Name", "Last Name", "Parents First Name", "Parents Last Name", "Date of Birth", "Admission Date", "Gender", "Email", "Phone No", "Street No", "House No", "Zip Code", "Class to Attend", "Room");
	        Coachdata    Coachreg2 = new Coachdata("Coacht Id", "First Name", "Last Name", "Parents First Name", "Parents Last Name", "Date of Birth", "Admission Date", "Gender", "Email", "Phone No", "Street No", "House No", "Zip Code", "Class to Attend", "Room");
	        Coachdata    Coachreg3 = new Coachdata("Coach Id", "First Name", "Last Name", "Parents First Name", "Parents Last Name", "Date of Birth", "Admission Date", "Gender", "Email", "Phone No", "Street No", "House No", "Zip Code", "Class to Attend", "Room");
	        Coachdata    Coachreg4 = new Coachdata("Coach Id", "First Name", "Last Name", "Parents First Name", "Parents Last Name", "Date of Birth", "Admission Date", "Gender", "Email", "Phone No", "Street No", "House No", "Zip Code", "Class to Attend", "Room");
	        Coachdata    Coachreg5 = new Coachdata("Coach Id", "First Name", "Last Name", "Parents First Name", "Parents Last Name", "Date of Birth", "Admission Date", "Gender", "Email", "Phone No","Street No", "House No", "Zip Code", "Class to Attend", "Room");
	        
	        
	        ArrayList <Coachdata> list =  Singleton.getInstance().getCoachdataList();			
		    
		    list.add(Coachreg1);
		    list.add(Coachreg2);
		    list.add(Coachreg3);
		    list.add(Coachreg4);
		    list.add(Coachreg5);
	        
	    } 
	} 