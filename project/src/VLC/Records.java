package VLC;




	import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import java.awt.BorderLayout;
	import java.awt.Image;
	import java.awt.Toolkit;
	import java.awt.event.*;
import java.util.ArrayList;
	
	
	public class Records {
		 JFrame frame;
		 JMenuBar menubar;
		 JMenu filemenu; JMenuItem Exit;
		 JMenu frames; JMenuItem studentregistration;  JMenuItem coachregistration;  JMenuItem appointment;  JMenuItem Attendence;
		 JLabel Records;    
		 JLabel Student_info;
		 
		 JLabel Coach_info;
		
		 JLabel Appointment_info;
		 
		 JLabel Attend_info;
		  
		 JTable table;
		 JScrollPane pane;
	     DefaultTableModel model;
		  
	     JButton load_1;	  JButton load_2;
	 	JButton load_3;	  JButton load_4;          JButton exit;    JButton mainpage;
    private JLabel label;
    private JLabel label_1;
    private JLabel label_2;
	     
	     
		 Records() 
		    { 
		         
		        frame=new JFrame("Venue Leisure Centre"); 
		        Image img = Toolkit.getDefaultToolkit().getImage("icon.PNG");
		        frame.setIconImage(img);
		        
		        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
		        
		        frame.setSize(1050, 655); 
		        
		        frame.setLocationRelativeTo(null);
		          
		        frame.getContentPane().setLayout(null);   
		          
		        frame.setVisible(true);
		        
		          
	            Records = new JLabel("RECORDS"); 
		        Records.setBounds(434, 11, 58, 26); 
		        frame.getContentPane().add(Records); 
		        
		        Student_info = new JLabel("Student Info"); 
		        Student_info.setBounds(212, 59, 78, 26); 
		        frame.getContentPane().add(Student_info);
		        	        
		      
		        
		        Coach_info = new JLabel("Coach Info"); 
		        Coach_info.setBounds(671, 59, 90, 26); 
		        frame.getContentPane().add(Coach_info);
		        	        	        
		       
		        	                    
		        Appointment_info = new JLabel("Appointment Info"); 
		        Appointment_info.setBounds(197, 327, 104, 26); 
		        frame.getContentPane().add(Appointment_info);
		        	        	        	        
		       
		        	        	        	        
		        Attend_info = new JLabel("Attendence Info"); 
		        Attend_info.setBounds(671, 327, 104, 26); 
		        frame.getContentPane().add(Attend_info);
	                        
	     //buttons
	                    		

	                    		load_1= new JButton("Student Record");
	                            load_1.addActionListener(new ActionListener() {
	                             	public void actionPerformed(ActionEvent arg0) {
	                             		
	                             		ArrayList<Studentdata> vector = Singleton.getInstance().getStudentdataList();
	           		   		            String[] columnNames = {"Student Id", "First Name", "Last Name", "Parents First Name", "Parents Last Name", "Date of Birth", "Date of Admission", "Gender", "Email", "Phone No", "Street No", "House No", "Zip Code", "Class to Attend"}; 

	           		   		  	 Object[][] data;
	           		   		  	 
	           					data = new Object[vector.size()][15];

	           					for (int i = 0; i < data.length; i++) {
	           						Studentdata a = vector.get(i);
	           						data[i][0] = a.getStudent_id();
	           						data[i][1] = a.getFname();
	           						data[i][2] = a.getLname();
	           						data[i][3] = a.getParent_First();
	           						data[i][4] = a.getParent_Last();
	           						data[i][5] = a.getDob_dc();
	           						data[i][6] = a.getDate_of_admission_dc();
	           						data[i][7] = a.getGender_group();
	           						data[i][8] = a.getEmail();
	           						data[i][9] = a.getphone_no();
	           						data[i][10] = a.getStreet_no();
	           						data[i][11] = a.getHouse_no();
	           						data[i][12] = a.getZip_code();
	           						data[i][13] = a.getClass_to_attend_cb();
	           						
	           						
	           						
	           					}
	           					
                				model = new DefaultTableModel(data, columnNames);
	           		   			table = new JTable(model);
	           		   			pane  = new JScrollPane(table);

	           		   			pane.setVisible(true);

	           		   			pane.setBounds(10, 84, 439, 232);

	        	   	           frame.getContentPane().add(pane);	
	           		                  
	           		                 }
	           		                 
	                           });	
                		
	                    		
	                    		load_1.setBounds(909, 97, 90, 25);
	                    		frame.getContentPane() .add(load_1);
	                    		

	                    		load_2= new JButton("Attendence Record");
	                    		load_2.addActionListener(new ActionListener() {
	                    	        	public void actionPerformed(ActionEvent arg0) {
	                    	        		ArrayList<Attendencedata> vector = Singleton.getInstance(). getAttendencedataList();
	                    	  		            String[] columnNames = {"id","Student Name", "Class Attendance", "Appointment Date", "Appointment Time"}; 

	                    	  		  	 Object[][] data;
	                    	  		  	 
	                    				data = new Object[vector.size()][5];

	                    				for (int i = 0; i < data.length; i++) {
	                    					Attendencedata a = vector.get(i);
	                    					
	                    					data[i][0] = a.getCoachid_tf();
	                    					data[i][1] = a.getStudent_name_tf();
	                    					data[i][2] = a.getClass_attend_cb();
	                    					data[i][3] = a.getAppointment_date_dc();
	                    					data[i][4] = a.getAppointment_time_tc();
	                    				}
	                    				model = new DefaultTableModel(data, columnNames);
	    	           		   			table = new JTable(model);
	    	           		   			pane  = new JScrollPane(table);

	    	           		   			pane.setVisible(true);

	    	           		   			pane.setBounds(499, 350, 416, 266);

	    	        	   	           frame.getContentPane().add(pane);	
	    	           		                  
	    	           		                 }
	    	           		                 
	    	                           });	
	                    		
	                    		load_2.setBounds(909, 141, 90, 25);
	                    		frame.getContentPane() .add(load_2);
	                    		
	                    		
	                    		load_3= new JButton("Coach Record");
	                    		load_3.addActionListener(new ActionListener() {
	                             	public void actionPerformed(ActionEvent arg0) {
	                             		
	                             		ArrayList<Coachdata> vector = Singleton.getInstance().getCoachdataList();
	           		   		            String[] columnNames = {"Coach Id", "First Name", "Last Name", "Parents First Name", "Parents Last Name", "Date of Birth", "Date of Admission", "Gender", "Email", "Phone No", "Street No", "House No", "Zip Code", "Class to Attend", "Room"}; 

	           		   		  	 Object[][] data;
	           		   		  	 
	           					data = new Object[vector.size()][14];

	           					for (int i = 0; i < data.length; i++) {
	           						Coachdata a = vector.get(i);
	           						data[i][0] = a.getCoach_id();
	           						data[i][1] = a.getFname();
	           						data[i][2] = a.getLname();
	           						data[i][3] = a.getParent_First();
	           						data[i][4] = a.getParent_Last();
	           						data[i][5] = a.getDob_dc();
	           						data[i][6] = a.getDate_of_admission_dc();
	           						data[i][7] = a.getGender_group();
	           						data[i][8] = a.getEmail();
	           						data[i][9] = a.getphone_no();
	           						data[i][10] = a.getStreet_no();
	           						data[i][11] = a.getHouse_no();
	           						data[i][12] = a.getZip_code();
	           						data[i][13] = a.getClass_to_attend_cb();
	           						data[i][14] = a.getroom_cb();
	           						
	           						
	           						
	           						
	           						
	           					}
	           		   				model = new DefaultTableModel(data, columnNames);
	           		   			table = new JTable(model);
	           		   			pane  = new JScrollPane(table);

	           		   			pane.setVisible(true);

	           		   			pane.setBounds(468, 82, 431, 243);

	        	   	           frame.getContentPane().add(pane);	
	           		                  
	           		                 }
	           		                 
	                           });	
	                    		
	                	        load_3.setBounds(909, 185, 90, 25);
	                    		frame.getContentPane() .add(load_3);
	                    		
	                    		load_4= new JButton("Appointment Record");
	                    		load_4.addActionListener(new ActionListener() {
	                    	        	public void actionPerformed(ActionEvent arg0) {
	                    	        		ArrayList<Appointmentdata> vector = Singleton.getInstance(). getAppointmentdataList();
	                    	  		            String[] columnNames = {"Coach id", "First Name", "Last Name", "Number", "Gender", "Room", "Appointment date", "Appointment time"}; 

	                    	  		  	 Object[][] data;
	                    	  		  	 
	                    				data = new Object[vector.size()][10];

	                    				for (int i = 0; i < data.length; i++) {
	                    					Appointmentdata a = vector.get(i);
	                    					
	                    					data[i][0] = a.getCoachid_tf();
	                    					data[i][1] = a.getFname_tf();
	                    					data[i][2] = a.getLname_tf();
	                    					data[i][3] = a.getNumber();
	                    					data[i][4] = a.getGender_group();
	                    					data[i][5] = a.getA_room_cb();
	                    					data[i][6] = a.getAppointment_date_dc();
	                    					data[i][7] = a.getAppointment_time_tc();
	                    				}
	                    					model = new DefaultTableModel(data, columnNames);
	        	           		   			table = new JTable(model);
	        	           		   			pane  = new JScrollPane(table);

	        	           		   			pane.setVisible(true);

	        	           		   			pane.setBounds(26, 352, 429, 253);

	        	        	   	           frame.getContentPane().add(pane);	
	        	           		                  
	        	           		                 }
	        	           		                 
	        	                           });	
	                    				
	                    		
	                	        load_4.setBounds(909, 228, 115, 25);
	                    		frame.getContentPane() .add(load_4);
	                    		

	                    		
	                    		
	                    		exit = new JButton("Exit");
	                    		exit.addActionListener(new ActionListener() {
	                    		public void actionPerformed(ActionEvent arg0) {
	                    		int result=JOptionPane.showConfirmDialog(frame, "are you sure you want to quit", "Exit System",JOptionPane.YES_NO_OPTION);	
	                    		if(result ==JOptionPane.YES_NO_OPTION)
	                    		
	                    		{
	                    			System.exit(0);
	                    		}
	                    		}});	
	                    		
	                    	
	                    		exit.setBounds(909, 300, 115, 25);
	                    		frame.getContentPane() .add(exit);
	                    		
	                    		mainpage= new JButton("Main");
	                    		mainpage.addActionListener(new ActionListener() {
	                    			public void actionPerformed(ActionEvent arg0) {
	                    			
	                    				Menu.main(null);
	                    				frame.dispose();
	                    					
	                    			}
	                    		});

	                    		
	                    		mainpage.setBounds(909, 264, 115, 25);
	                    		frame.getContentPane() .add(mainpage);
	                    		


	                    		
	                    		
		    } 
		 
		
		      
		    public static void main(String[] args) { 
		        new Records(); 
		        
		    } 
	}

