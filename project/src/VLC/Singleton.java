package VLC;

import java.util.*;
import java.lang.reflect.Array;

public class Singleton {

	private static Singleton adminobject = new Singleton();	
// All admin users will be stored in the following map.
	private Map<String, Admin> admin_user_list = new HashMap<> (); 
	private ArrayList<Appointmentdata> App_list = new ArrayList ();
	private ArrayList<Studentdata> Student_list = new ArrayList ();
	private ArrayList<Coachdata> Coach_list = new ArrayList ();
	private ArrayList<Attendencedata> Attendence_list = new ArrayList ();
	
	
	private Singleton(){}
	public static Singleton getInstance(){
		return adminobject;
	}
	public Map<String, Admin> getAdminUserList(){
		return admin_user_list;
		}
	
	public ArrayList<Appointmentdata> getAppointmentdataList(){
		return App_list;
		
		
	}
	
	public ArrayList<Studentdata> getStudentdataList(){
		return Student_list;
		
		
	}
	
	public ArrayList<Coachdata> getCoachdataList(){
		return Coach_list;
		
		
	}
	
	public ArrayList<Attendencedata> getAttendencedataList(){
		return Attendence_list;
		
		
	}
}
