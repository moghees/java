package VLC;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.Container;
import java.util.ArrayList;
import java.util.Map;
import java.awt.event.*; import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import com.toedter.calendar.JDateChooser;
import lu.tudor.santec.jtimechooser.JTimeChooser;

import javax.swing.*;

public class Attendence {
	JFrame frame; 
    JLabel Attendence;
    JLabel Coach_id;
    JTextField Coachid_tf;
    JLabel Student_name;
    JTextField Student_name_tf;
    JLabel Appointment_d;
    JDateChooser Appointment_date_dc;
    JLabel Appointment_t;
    JTimeChooser Appointment_time_tc;
	JLabel Class_attend;             
	JComboBox Class_attend_cb;
    JButton Save_btn;
    JButton Back_btn;
    JButton Clear_btn;
    JTable Table;
    JLabel Logo;
    JButton Load_btn;
    DefaultTableModel modell;
    JTable tab;
    JScrollPane scroll;
    
    Attendence(){
    	frame=new JFrame("Venue Liesure Center");
    	
    	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
        
        frame.setSize(650, 414); 
        
        frame.setLocationRelativeTo(null);
          
        frame.getContentPane().setLayout(null);   
          
        frame.setVisible(true);
        
        Attendence = new JLabel("Attendence"); 
        
        Attendence.setBounds(256, 11, 126, 26); 
                 
        frame.getContentPane().add(Attendence);
        
        
    	Coach_id = new JLabel("Coach ID"); 
        
		Coach_id.setBounds(39, 58, 95, 16); 
  
		frame.getContentPane().add(Coach_id);

    	Coachid_tf = new JTextField(); 

    	Coachid_tf.setBounds(144, 55, 210, 22);
  
    	frame.getContentPane().add(Coachid_tf);
    	
    	
    	 Student_name = new JLabel("Student Name"); 
         
         Student_name.setBounds(39, 95, 95, 16); 
           
         frame.getContentPane().add(Student_name);
         
         Student_name_tf = new JTextField(); 
         
         Student_name_tf.setBounds(144, 92, 210, 22);
           
         frame.getContentPane().add(Student_name_tf);
         
          
		   Appointment_d = new JLabel("Appoinment Date"); 
	          
	       Appointment_d.setBounds(39, 133, 98, 20); 
	                   
	       frame.getContentPane().add(Appointment_d);

        Appointment_date_dc = new JDateChooser();
        
	      Appointment_date_dc.setBounds(144, 133, 210, 20);
	      
	     
    	  frame.getContentPane().add(Appointment_date_dc);
    	  

    	  Appointment_t = new JLabel("Appoinment Time"); 
          
	       Appointment_t.setBounds(39, 164, 98, 20); 
	                   
	       frame.getContentPane().add(Appointment_t);
    	  Appointment_time_tc = new JTimeChooser();
	        
          Appointment_time_tc.setBounds(148, 162, 110, 20);
          
      	  frame.getContentPane().add(Appointment_time_tc);
      	  
      	  
      	 Class_attend = new JLabel("Class Attendence");
	        
         Class_attend.setBounds(37, 198, 97, 26);
	        
         frame.getContentPane().add(Class_attend);
         
         Class_attend_cb = new JComboBox();
	        
         Class_attend_cb.setModel(new DefaultComboBoxModel(new String[] {"PRESENT", "ABSENT"}));
	        
         Class_attend_cb.setBounds(144, 198, 87, 26);
			
	        frame.getContentPane().add(Class_attend_cb);
      	  
    	  
        
        Save_btn = new JButton("Save"); 
        Save_btn.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		 ArrayList<Attendencedata> al = Singleton.getInstance(). getAttendencedataList();
    		 Attendencedata fbd = new Attendencedata(Coachid_tf.getText(), Student_name_tf.getText(), Class_attend_cb.getSelectedItem().toString(), Appointment_date_dc.getDate().toString(), Appointment_time_tc.getTimeField().getText());
        	 
	                	 al.add(fbd);
	                	JOptionPane.showMessageDialog(null, "Data Entered Successfully");  
        	}
        });
        
        Save_btn.setBounds(498, 100, 126, 22);
                 
        frame.getContentPane().add(Save_btn);

        
        Load_btn = new JButton("Load data"); 
        Load_btn.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		ArrayList<Attendencedata> vector = Singleton.getInstance(). getAttendencedataList();
  		            String[] columnNames = {"Coach id","Student Name", "Class Attendance", "Appointment Date", "Appointment Time"}; 

  		  	 Object[][] data;
  		  	 
			data = new Object[vector.size()][5];

			for (int i = 0; i < data.length; i++) {
				Attendencedata a = vector.get(i);
				
				data[i][0] = a.getCoachid_tf();
				data[i][1] = a.getStudent_name_tf();
				data[i][2] = a.getClass_attend_cb();
				data[i][3] = a.getAppointment_date_dc();
				data[i][4] = a.getAppointment_time_tc();
			}
  				modell = new DefaultTableModel(data, columnNames);
  			tab = new JTable(modell);
  		scroll  = new JScrollPane(tab);

  		scroll.setVisible(true);

  		scroll.setBounds(10, 238, 614, 126);

                    frame.getContentPane().add(scroll);	
                 
        	}
        });
        
        Load_btn.setBounds(498, 67, 126, 22);
                 
        frame.getContentPane().add(Load_btn);
               
        
        Back_btn = new JButton("Back"); 
        Back_btn.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		Menu.main(null);
        		frame.dispose();
        	}
        });
               
        Back_btn.setBounds(498, 133, 126, 22);
                        
        frame.getContentPane().add(Back_btn);
        
        
        
        Clear_btn = new JButton("Clear"); 
        Clear_btn.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		
        	    Coachid_tf.setText("");
        	    Student_name_tf.setText("");
        	    
        	    Class_attend_cb.setSelectedIndex(0);

        	    
        	    
        		}
        });
        Clear_btn.setBounds(501, 166, 123, 22);                
        frame.getContentPane().add(Clear_btn);
        
        Table = new JTable();
        Table.setBounds(24, 455, 899, 427);
        frame.getContentPane().add(Table);
                     
    	
    }
    
    public static void main(String[] args) { 
        new Attendence(); 
        Attendencedata ad1 = new Attendencedata("1","jill", "Class Attendance", "Appointment Date", "Appointment Time"); 
        Attendencedata ad2 = new Attendencedata("2","jack", "Class Attendance", "Appointment Date", "Appointment Time"); 
        Attendencedata ad3 = new Attendencedata("3","tom", "Class Attendance", "Appointment Date", "Appointment Time"); 
        Attendencedata ad4 = new Attendencedata("4","adam", "Class Attendance", "Appointment Date", "Appointment Time"); 
        Attendencedata ad5 = new Attendencedata("5","moses", "Class Attendance", "Appointment Date", "Appointment Time"); 
        
        
        ArrayList <Attendencedata> list =  Singleton.getInstance().getAttendencedataList();			
	    
	    list.add(ad1);
	    list.add(ad2);
	    list.add(ad3);
	    list.add(ad4);
	    list.add(ad5);
        
    } 

}
