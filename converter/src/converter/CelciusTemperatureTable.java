/*
 Write a program to generate a table of temperature conversions, converting from fahrenheit to celsius:

    Write a function to convert a single temperature from fahrenheit to celsius. If F is the temperature in degrees fahrenheit, the temperature in degrees celsius is (F-32)*5/9.

    Write a function which takes a start, end and step size, and prints a table of temperature conversions. For example, if you call it with start 5, end 10 and step 2, it will print a line for 5, 7 and 9.

    Call your functions with different values, print the results, and make sure they are working correctly.

 */

package converter;

public class CelciusTemperatureTable {
	
	public static double celsius(double farhenhietTemperature) {
		double celsius;
		celsius = (farhenhietTemperature-32)*((double)5/9); /*the double has been added next to the 5 because Java 5/9 as an integer giving the return value to 0, so the 5/9 return value is a double */
		return celsius;
	}
	
	public static void table() {
		System.out.println("Fahrenhiet Temperature \t\t Celcius Temperature \n----------\t\t\t\t----------\n");
	}
	
	public static void main (String [] args) {
		table();
		double celciusEq;
		for (double currentFahrenhietTemperature = 5; currentFahrenhietTemperature <= 10; currentFahrenhietTemperature+=2) {
			celciusEq = celsius(currentFahrenhietTemperature);
			System.out.printf( "%.1f\t\t\t\t%.2f\n",currentFahrenhietTemperature, celciusEq);
		}
		
	}

}
